package universidad.project.mototaxis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import universidad.project.mototaxis.domains.Viaje;

import java.util.List;


public interface IViajesDao extends JpaRepository<Viaje, Long> {

    // Mostrar viajes por id del piloto
    @Query("SELECT v FROM Viaje v WHERE v.vehiculo.piloto.id = ?1")
    List<Viaje> verViajesPorIdPiloto(Long id);

    // Mostrar viajes por id del pasajero
    @Query("SELECT v FROM Viaje v WHERE v.pasajero.id = ?1")
    List<Viaje> verViajesPorIdPasajero(Long id);

}
