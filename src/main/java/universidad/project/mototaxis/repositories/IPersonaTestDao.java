package universidad.project.mototaxis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import universidad.project.mototaxis.domains.Person;
import universidad.project.mototaxis.domains.Persona;

//Extiende de JpaRepository
// Contiene las operaciones crud para tipos de objetos
public interface IPersonaTestDao extends JpaRepository<Person, Long> {
}
