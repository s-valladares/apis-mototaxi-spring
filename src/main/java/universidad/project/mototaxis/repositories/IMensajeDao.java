package universidad.project.mototaxis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import universidad.project.mototaxis.domains.Mensaje;

public interface IMensajeDao extends JpaRepository<Mensaje, Long> {
}
