package universidad.project.mototaxis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import universidad.project.mototaxis.domains.Persona;

//Extiende de JpaRepository
// Contiene las operaciones crud para tipos de objetos
public interface IPersonaDao extends JpaRepository<Persona, Long> {
}
