package universidad.project.mototaxis.services;

import com.google.firebase.messaging.FirebaseMessagingException;

public interface INotificationService {

    void sendToken(String mensaje) throws FirebaseMessagingException;
}
