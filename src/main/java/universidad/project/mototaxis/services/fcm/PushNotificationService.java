package universidad.project.mototaxis.services.fcm;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import universidad.project.mototaxis.domains.fcm.PushNotificationRequest;

@Service
public class PushNotificationService {
    private Logger logger = LoggerFactory.getLogger(PushNotificationService.class);
    private FCMService fcmService;
    public PushNotificationService(FCMService fcmService) {
        this.fcmService = fcmService;
    }
    public void sendPushNotification(PushNotificationRequest request) {

        try {
            fcmService.sendMessage(request.getData(), request);

        } catch (Exception e) {
            logger.error(e.getMessage());

        }

    }
    public void sendPushNotificationWithoutData(PushNotificationRequest request) {
        try {
            fcmService.sendMessageWithoutData(request);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }
    public String sendPushNotificationToToken(PushNotificationRequest request) {
        String resp = "";
        try {
            fcmService.sendMessageToToken(request);
            resp = "Enviado " + request.getMessage();
        } catch (Exception e) {
            logger.error(e.getMessage());
            resp = e.getMessage();
        }
        return resp;
    }
    private Map<String, String> getSamplePayloadData() {
        Map<String, String> pushData = new HashMap<>();
        pushData.put("messageId", "msgid");
        pushData.put("text", "txt");
        pushData.put("user", "pankaj singh");
        return pushData;
    }
}