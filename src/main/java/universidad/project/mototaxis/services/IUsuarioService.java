package universidad.project.mototaxis.services;

import universidad.project.mototaxis.domains.Persona;
import universidad.project.mototaxis.domains.Usuario;

import java.util.List;

public interface IUsuarioService {
    //Métodos crud del objeto Usuario
    List<Usuario> getAll(); //Devuelve listado de usuarios
    Usuario getId(Long id); //Devuelve usuario por id
    Usuario create(Usuario p); //Crea un usuaruio
    void delete(Long id); //Elimina un usuario

    //Consultas personalizadas
    Usuario findByEmail(String email);
    Persona findPersonaByUsuarioId(Long id);
}
