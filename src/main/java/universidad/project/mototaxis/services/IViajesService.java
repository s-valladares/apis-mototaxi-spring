package universidad.project.mototaxis.services;

import universidad.project.mototaxis.domains.Piloto;
import universidad.project.mototaxis.domains.Viaje;

import java.util.List;

public interface IViajesService {
    List<Viaje> getAll();
    Viaje getId(Long id);
    Viaje create(Viaje p);
    void delete(Long id);
    List<Viaje> verViajesPorIdPiloto(Long id);
    List<Viaje> verViajesPorIdPasajero(Long id);
}
