package universidad.project.mototaxis.services;

import universidad.project.mototaxis.domains.Mensaje;

import java.util.List;

public interface IMensajeService {
    List<Mensaje> getAll();
    Mensaje getId(Long id);
    Mensaje create(Mensaje p);
    void delete(Long id);

}
