package universidad.project.mototaxis.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import universidad.project.mototaxis.domains.Piloto;
import universidad.project.mototaxis.domains.Viaje;
import universidad.project.mototaxis.repositories.IPilotoDao;
import universidad.project.mototaxis.repositories.IViajesDao;
import universidad.project.mototaxis.services.IPilotoService;
import universidad.project.mototaxis.services.IViajesService;

import java.util.List;

@Service
public class ViajesImpl implements IViajesService {
    @Autowired
    private IViajesDao objDao;

    @Override
    @Transactional(readOnly = true)
    public List<Viaje> getAll() {
        return (List<Viaje>) objDao.findAll();
    }

    @Override
    public Viaje getId(Long id) {
        return objDao.findById(id).orElse(null);
    }

    @Override
    public Viaje create(Viaje obj) {
        return objDao.save(obj);
    }

    @Override
    public void delete(Long id) {
        objDao.deleteById(id);
    }

    @Override
    public List<Viaje> verViajesPorIdPiloto(Long id) {
        return objDao.verViajesPorIdPiloto(id);
    }

    @Override
    public List<Viaje> verViajesPorIdPasajero(Long id) {
        return objDao.verViajesPorIdPasajero(id);
    }


}
