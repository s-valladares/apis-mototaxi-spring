package universidad.project.mototaxis.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import universidad.project.mototaxis.domains.Mensaje;
import universidad.project.mototaxis.repositories.IMensajeDao;
import universidad.project.mototaxis.services.IMensajeService;

import java.util.List;

@Service
public class MensajeImpl implements IMensajeService {

    @Autowired
    IMensajeDao objDao;

    @Override
    public List<Mensaje> getAll() {
        return objDao.findAll();
    }

    @Override
    public Mensaje getId(Long id) {
        return objDao.findById(id).orElse(null);
    }

    @Override
    public Mensaje create(Mensaje p) {
        return objDao.save(p);
    }

    @Override
    public void delete(Long id) {
        objDao.deleteById(id);
    }
}
