package universidad.project.mototaxis.domains;

import universidad.project.mototaxis.config.AuditModel;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity //Indica que este obejeto es una entidad
@Table(name = "personas_test") //Indica el nombre de la tabla en la base de datos
public class Person extends AuditModel implements Serializable {

    @Id //Clave principal
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Autoincrementable
    private Long id;

    //Nombres de campos en la base de datos
    @NotEmpty
    @Size(min = 1, max = 20)
    @Column(name = "nombres", nullable = false, length = 20)
    private String nombres;
    private String apellidos;

    @Column(name = "dpi", nullable = false, length = 13, unique = true)
    @NotEmpty
    @Size(min = 13, max = 13)
    private String dpi;
    private String municipio;
    private String aldea;


    private String telefono;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDpi() {
        return dpi;
    }

    public void setDpi(String dpi) {
        this.dpi = dpi;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getAldea() {
        return aldea;
    }

    public void setAldea(String aldea) {
        this.aldea = aldea;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /** MÉTODOS DE ACESO A DATOS**/


    private static final long serialVersionUID = 1L;
}
