package universidad.project.mototaxis.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.lang.NonNull;
import universidad.project.mototaxis.config.AuditModel;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

@Entity //Indica que este obejeto es una entidad
@Table(name = "usuarios") //Indica el nombre de la tabla en la base de datos
public class Usuario extends AuditModel implements Serializable {

    @Id //Clave principal
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Autoincrementable
    private Long id;

    //Nombres de campos en la base de datos
    // Tabla personas relacionada con usuarios por su id
    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn
    @JsonIgnoreProperties({ "updatedAt", "hibernateLazyInitializer", "handler" })
    @NonNull
    private Persona persona;
    @NotEmpty
    @NonNull
    private String email;

    @NonNull
    private String password;
    @Column(name = "token_fcm")
    private String tokenFCM;
    private Boolean enabled;
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Rol> roles;

    //Métodos de acceso a datos
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Persona getPersona() {
        return persona;
    }
    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getTokenFCM() {
        return tokenFCM;
    }
    public void setTokenFCM(String token) {
        this.tokenFCM = token;
    }
    public Boolean getEnabled() {
        return enabled;
    }
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
    public List<Rol> getRoles() {
        return roles;
    }
    public void setRoles(List<Rol> roles) {
        this.roles = roles;
    }
    private static final long serialVersionUID = 1L;
}
