package universidad.project.mototaxis.domains;

import org.springframework.lang.NonNull;
import universidad.project.mototaxis.config.AuditModel;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Entity //Indica que este obejeto es una entidad
@Table(name = "personas") //Indica el nombre de la tabla en la base de datos
public class Persona extends AuditModel implements Serializable {

    @Id //Clave principal
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Autoincrementable
    private Long id;

    //Nombres de campos en la base de datos

    private String nombres;
    private String apellidos;
    private String direccion;
    private String telefono;

    /** MÉTODOS DE ACESO A DATOS**/
    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getNombres() { return nombres; }

    public void setNombres(String nombres) { this.nombres = nombres; }

    public String getApellidos() { return apellidos; }

    public void setApellidos(String apellidos) { this.apellidos = apellidos; }

    public String getDireccion() { return direccion; }

    public void setDireccion(String direccion) { this.direccion = direccion; }

    public String getTelefono() { return telefono; }

    public void setTelefono(String telefono) { this.telefono = telefono; }

    private static final long serialVersionUID = 1L;
}
