package universidad.project.mototaxis.controllers;

import org.hibernate.exception.DataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import universidad.project.mototaxis.config.UrlBaseApi;
import universidad.project.mototaxis.domains.Mensaje;
import universidad.project.mototaxis.domains.Piloto;
import universidad.project.mototaxis.services.IMensajeService;
import universidad.project.mototaxis.services.IUsuarioService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping(UrlBaseApi.URL_API)
public class MensajeController {
    private final String servicio = "/mensajes";

    @Autowired
    private IMensajeService mensajeService;
    Map<String, Object> response = new HashMap<>();

    @PostMapping(servicio)
    public ResponseEntity<?> createMessageChat(
            @Valid
            @RequestBody Mensaje msj,
            BindingResult result
            ) {

        Mensaje mensaje;

        if (result.hasErrors()) {
            List<String> errors = result
                    .getFieldErrors()
                    .stream().map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
                    .collect(Collectors.toList());

            response.put("errors", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        try {
            mensaje = mensajeService.create(msj);
        } catch (DataAccessException ex) {
            response.put("mensaje", "Error al insertar en la base de datos");
            response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("Mensaje", "Ok");
        response.put("RES", mensaje);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping(servicio + "/all")
    public ResponseEntity<?> getAllMensajes() {
        response.clear();
        List<Mensaje> objNew;

        try {
            objNew = mensajeService.getAll();
        } catch (DataAccessException ex) {
            response.put("mensaje", "Error al obtener de la base de datos");
            response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("size", objNew.size());
        response.put("rows", objNew);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
