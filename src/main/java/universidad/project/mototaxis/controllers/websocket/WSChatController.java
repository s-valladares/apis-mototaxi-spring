package universidad.project.mototaxis.controllers.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import universidad.project.mototaxis.domains.Mensaje;
import universidad.project.mototaxis.domains.Ubicacion;
import universidad.project.mototaxis.services.IMensajeService;
import universidad.project.mototaxis.services.IUbicacionService;

import java.util.HashMap;
import java.util.Map;

@Controller
public class WSChatController {

    @Autowired
    private IUbicacionService objService;

    @Autowired
    private IMensajeService mensajeService;

    private Logger log = LoggerFactory.getLogger(WSChatController.class);

    Map<String, Object> response = new HashMap<>();
    Ubicacion objNew = null;

    @MessageMapping("/mensajes")
    @SendTo("/ubicaciones/mensajes")
    public ResponseEntity<?> sendMessage(Mensaje mensaje) {

        response.clear();
        Mensaje msj;
        try {
            msj = mensajeService.create(mensaje);

        } catch (DataAccessException ex) {
            response.put("mensaje", "Error al insertar en la base de datos");
            response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "OK");
        response.put("RES", msj);
        return new ResponseEntity<>(response, HttpStatus.OK);

    }


}
