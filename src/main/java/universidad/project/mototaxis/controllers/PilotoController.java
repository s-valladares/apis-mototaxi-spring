package universidad.project.mototaxis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import universidad.project.mototaxis.config.UrlBaseApi;
import universidad.project.mototaxis.domains.Piloto;
import universidad.project.mototaxis.domains.Ubicacion;
import universidad.project.mototaxis.services.IPilotoService;
import universidad.project.mototaxis.services.IUbicacionService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping(UrlBaseApi.URL_API)
public class PilotoController {

    private final String entidad = "/pilotos";
    Map<String, Object> response = new HashMap<>();

    @Autowired
    private IPilotoService objService;

    @Autowired
    private IUbicacionService ubicacionService;


    @GetMapping(entidad)
    public ResponseEntity<?> index() {

        response.clear();
        List<Piloto> objNew;

        try {
            objNew = objService.verPilotoDisponible();
        } catch (DataAccessException ex) {
            response.put("mensaje", "Error al obtener de la base de datos");
            response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("size", objNew.size());
        response.put("rows", objNew);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(entidad + "/all")
    public ResponseEntity<?> getAllPilotos() {

        response.clear();
        List<Piloto> objNew;

        try {
            objNew = objService.getAll();
        } catch (DataAccessException ex) {
            response.put("mensaje", "Error al obtener de la base de datos");
            response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("size", objNew.size());
        response.put("rows", objNew);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(entidad + "/usuario/{id}" )
    public ResponseEntity<?> verPilotoIdUsuario(@PathVariable Long id) {

        response.clear();
        Piloto obj;

        try {
            obj = objService.verPilotoPorIdUsuario(id);
        } catch (DataAccessException ex) {
            response.put("mensaje", "Error al consultar base de datos");
            response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (obj == null) {
            response.put("error", true);
            response.put("mensaje", "NO hay PILOTO para el ID: " + id);
            response.put("RES", obj);
            return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
        }

        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PostMapping(entidad)
    public ResponseEntity<?> create(@Valid @RequestBody Piloto x, BindingResult result) {

        response.clear();
        Piloto objNew;

        if (result.hasErrors()) {
            List<String> errors = result
                    .getFieldErrors()
                    .stream().map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
                    .collect(Collectors.toList());

            response.put("errors", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        try {
            objNew = objService.create(x);

        } catch (DataAccessException ex) {
            response.put("mensaje", "Error al insertar en la base de datos");
            response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "OK");
        response.put("RES", objNew);

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PostMapping(entidad  + "/activar")
    public ResponseEntity<?> activarPiloto(@Valid @RequestBody Ubicacion ub, BindingResult result) {

        response.clear();
        Ubicacion ubNew;

        if (result.hasErrors()) {
            List<String> errors = result
                    .getFieldErrors()
                    .stream().map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
                    .collect(Collectors.toList());

            response.put("errors", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        try {
            System.out.println("Llegamos aquí");
            ubNew = ubicacionService.createUbicacionAndUpdatePiloto(ub, true);

        } catch (DataAccessException ex) {
            response.put("mensaje", "Error al insertar en la base de datos");
            response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "OK");
        response.put("RES", ubNew);

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }
/*
    @PostMapping(entidad + "/activar")
    public ResponseEntity<?> updatePilotoOn(Ubicacion ub) {
        System.out.println(ub.getUsuario().getId());
        System.out.println(ub.getLatitud());
        System.out.println(ub.getLongitud());
        response.clear();
        Ubicacion objNew = null;
        try {
            objNew = ubicacionService.createUbicacionAndUpdatePiloto(ub, true);

        } catch (DataAccessException ex) {
            response.put("mensaje", "Error al insertar en la base de datos");
            response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "OK");
        response.put("RES", objNew);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }*/
}
