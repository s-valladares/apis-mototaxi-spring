package universidad.project.mototaxis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import universidad.project.mototaxis.config.UrlBaseApi;
import universidad.project.mototaxis.domains.Persona;
import universidad.project.mototaxis.domains.Usuario;
import universidad.project.mototaxis.services.IUsuarioService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping(UrlBaseApi.URL_API)
public class UsuarioController {

    private final String entidad = "/usuarios";
    @Autowired
    private IUsuarioService objService;
    Map<String, Object> response = new HashMap<>();

    //Método para insertar usuario por meedio de la url /usuarios
    @PostMapping(entidad)
    public ResponseEntity<?> create(@Valid @RequestBody Usuario x, BindingResult result) {
        response.clear();
        Usuario objNew;

        if (result.hasErrors()) {
            List<String> errors = result
                    .getFieldErrors()
                    .stream().map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
                    .collect(Collectors.toList());

            response.put("errors", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        try {

            objNew = objService.create(x);

        } catch (DataAccessException ex) {
            response.put("mensaje", "Error al insertar en la base de datos");
            response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "OK");
        response.put("RES", objNew);

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping(entidad)
    public ResponseEntity<?> index() {
        response.clear();
        List<Usuario> objNew;

        try {
            objNew = objService.getAll();
        } catch (DataAccessException ex) {
            response.put("mensaje", "Error al obtener de la base de datos");
            response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("size", objNew.size());
        response.put("rows", objNew);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(entidad + "/{id}")
    public ResponseEntity<?> show(@PathVariable Long id) {
        response.clear();
        Usuario obj;

        try {
            obj = objService.getId(id);
        } catch (DataAccessException ex) {
            response.put("mensaje", "Error al consultar base de datos");
            response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (obj == null) {
            response.put("mensaje", entidad.toUpperCase() + " ID: ".concat(id.toString().concat(" No existe en la base de datos")));
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(obj, HttpStatus.OK);
    }



    @PutMapping(entidad + "/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody Usuario usuario, BindingResult result, @PathVariable Long id) {
        response.clear();
        Usuario flActual = objService.getId(id);
        Map<String, Object> response = new HashMap<>();

        if (result.hasErrors()) {
            List<String> errors = result
                    .getFieldErrors()
                    .stream().map(err -> "El campo '" + err.getField() + "'" + err.getDefaultMessage())
                    .collect(Collectors.toList());

            response.put("errors", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        if (flActual == null) {
            response.put("mensaje", "Error: no hay " + entidad + " con id: "
                    .concat(id.toString().concat(". No existe en la base de datos")));
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

        try {

            flActual.setTokenFCM(usuario.getTokenFCM());
            objService.create(flActual);

        } catch (DataAccessException ex) {
            response.put("mensaje", "Error actualizar el " + entidad + " en la base de datos");
            response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        System.out.println(flActual.getTokenFCM());
        response.put("mensaje", "El " + "usuario" + " fue actualizado correctamente");
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }
}
