package universidad.project.mototaxis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import universidad.project.mototaxis.config.UrlBaseApi;
import universidad.project.mototaxis.domains.Piloto;
import universidad.project.mototaxis.domains.Ubicacion;
import universidad.project.mototaxis.domains.Viaje;
import universidad.project.mototaxis.services.IPilotoService;
import universidad.project.mototaxis.services.IUbicacionService;
import universidad.project.mototaxis.services.IViajesService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping(UrlBaseApi.URL_API)
public class ViajeController {

    private final String entidad = "/viajes";
    Map<String, Object> response = new HashMap<>();

    @Autowired
    private IViajesService objService;

    @GetMapping(entidad + "/piloto/{id}" )
    public ResponseEntity<?> verViajesIdPiloto(@PathVariable Long id) {

        response.clear();
        List<Viaje> obj;

        try {
            obj = objService.verViajesPorIdPiloto(id);
        } catch (DataAccessException ex) {
            response.put("mensaje", "Error al consultar base de datos");
            response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (obj == null) {
            response.put("error", true);
            response.put("mensaje", "NO hay Viajes para el Piloto con este id : " + id);
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

        response.put("size", obj.size());
        response.put("rows", obj);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(entidad + "/pasajero/{id}" )
    public ResponseEntity<?> verViajesIdPasajero(@PathVariable Long id) {

        response.clear();
        List<Viaje> obj;

        try {
            obj = objService.verViajesPorIdPasajero(id);
        } catch (DataAccessException ex) {
            response.put("mensaje", "Error al consultar base de datos");
            response.put("error", ex.getMessage().concat(": ").concat(ex.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (obj == null) {
            response.put("error", true);
            response.put("mensaje", "NO hay Viajes para el pasjero con este id : " + id);
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

        response.put("size", obj.size());
        response.put("rows", obj);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}
